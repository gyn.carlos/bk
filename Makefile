.DEFAULT_GOAL := invalid

invalid:
	echo "Command inv�lid";
	exit;

# Instalando depend�ncia
deps:
	sudo docker load < images/estiling-v1.0.tar

#Starting app
up:
	sudo docker-compose up -d

#Stopping app
down:
	sudo docker-compose down --remove-orphans

#backup
backup:
	sudo docker exec -it estiling bash /var/lib/postgresql/bin/backup.sh

#restore
restore:
	sudo docker exec -it estiling bash /var/lib/postgresql/bin/restore.sh
