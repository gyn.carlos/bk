#!/bin/bash

dotenv()
{
    ENV_FILE=$PWD/bin
    set -a  
    [ -f $ENV_FILE/.env ] && . $ENV_FILE/.env
    set +a
}

dotenv;

test_connection() 
{
    pg_isready -d $DATABASE_NAME -h "$HOST" -p $PORT -U $USER_NAME -q
    connection_status=$?
    # 0: Connection OK
    if [ $connection_status != 0 ]
    then
        echo "Failed to connect to postgresql"
        exit
    fi
}

test_connection

main()
{
    # Get last backup
    BACKUP_FILE=$(ls -Art "$BACKUP_DIR" | tail -n 1)

    # Restoring data and saving the command output in the directory
    pv "$BACKUP_DIR/$BACKUP_FILE" | gunzip | PGPASSWORD=$PASSWORD pg_restore -h $HOST -p $PORT -U $USER_NAME -d $DATABASE_NAME 2>$DIR_BIN/$UUID.restore.log

    # Checking the logging the restore and view first 10 errors
    if [ -s "$DIR_BIN/$UUID.restore.log" ]
    then
        echo "ERROR";
        cat $DIR_BIN/$UUID.restore.log | head -n 10
    else
        echo "SUCCESSFUL";
    fi
}

main