#!/bin/bash

dotenv()
{
    ENV_FILE=$PWD/bin
    set -a  
    [ -f $ENV_FILE/.env ] && . $ENV_FILE/.env
    set +a
}

dotenv;

test_connection()
{
    pg_isready -d $DATABASE_NAME -h "$HOST" -p $PORT -U $USER_NAME -q
    connection_status=$?
    # 0: Connection OK
    if [ $connection_status != 0 ]
    then
        echo "Failed to connect to postgresql"
        exit
    fi
}

test_connection

create_email() 
{

echo "to: $RECIPIENT
From: $FROM
MIME-Version: 1.0
Content-Type: text/html; charset=\"ISO-8859-1\";
Subject: $SUBJECT

<html>
<body>
<h2>$TABLE_OPERATION_ID _uuid_operation_</h2>
<table>
    <caption><strong>$TABLE_CAPTION</strong></caption>
    <thead>
        <tr>
            <th align='center'>$TABLE_STARTED</th>
            <th align='center'>$TABLE_TIME_SPENT</th>
            <th align='center'>$TABLE_LENGTH_FILE</th>
        </tr>
    </thead
    <tbody>
        <tr>
            <td align='center'>`date +%d-%m-%Y" "%H:%M:%S`</td>
            <td align='center'>_time_spent_</td>
            <td align='center'>_file_length_</td>
        </tr>
    </tbody>
</table>
" > "$HISTORY"/$UUID.dump.log
}

create_email

dump() 
{
    PGPASSWORD=$PASSWORD pg_dump -h $HOST -p $PORT -U $USER_NAME -Fc -Z 9 -d $DATABASE_NAME
}

spin()
{
    pv -s $(PGPASSWORD=$PASSWORD psql -h $HOST -p $PORT -U $USER_NAME -tc "SELECT pg_database_size('$DATABASE_NAME')")
}

compress()
{
  pigz -c > $BACKUP_DIR/$BACKUP_FILE
}

main()
{
    START_TIME=$(date +%s%3N)
    dump | spin | compress
    ELAPSED_TIME=$(expr $(date +%s%3N) - $START_TIME)
    SECONDSS="$(awk 'BEGIN{print '$ELAPSED_TIME' / 1000}')"
    FILE_SIZE_MB="$(du -h $BACKUP_DIR/$BACKUP_FILE | cut -f1)"

    printf "Command finished in $SECONDSS seconds \n"
}

main

replace_values() 
{
    sed -i "s/_time_spent_/$SECONDSS/g;s/_uuid_operation_/$UUID/g;s/_file_length_/$FILE_SIZE_MB/g;" "$HISTORY"/$UUID.dump.log
}

replace_values

send_email() 
{
    echo "Sending mail ..."    
    cat "$HISTORY"/$UUID.dump.log | ssmtp $RECIPIENT
}

send_email