# BK Estilingue

BK Estilingue é um app de backup e restore lógico do postgres v9.5. Esta ativo em alguns postos de combustiveis da Linx, atual Stone.

gerenciar seus postos de combustiveis. 

  - Realiza o backup do banco
  - Notifica um usuário configurado no app da realização do backup
  - Restaura o backup realizado.

### Instalação

BK Estilingue requer [Docker](https://docs.docker.com/engine/install/ubuntu//) v19+ seja instalado.

1 Passo - Baixar e descompactar o app

[Download](https://gitlab.com/gyn.carlos/bk/-/archive/v1.0/bk-v1.0.zip)


2 Passo - Instalar as dependências do app

```sh
$ cd bk
$ make deps
```

Ao término do comando deve aparecer a seguinte mensagem no console: **Loaded image: estiling:v1.0**

### Configuração

Antes de executar o app as variáveis de ambiente devem ser configuradas primeiro no arquivo bk/bin/.env

1 Passo - Configurar variáveis de ambiente (segue um exemplo)
```sh
$ cd bk
$ cd bk/bin/
$ nano .env

#Caminho da aplicacao (nao mexer)
DIR_BIN=/var/lib/postgresql/bin

#Dados do cliente
CLIENT_ID="GEMA"

#Dados da conexao com o banco
HOST=192.168.15.4
PORT=5432
DATABASE_NAME=autosystem
USER_NAME=postgres
PASSWORD="Lider@201034linxgyn"

#Dados do backup
BACKUP_FILE=file_`date +%d-%m-%Y"_"%H_%M`.sql.gz
BACKUP_DIR=/var/lib/postgresql/backups

#Dados do cabecalho do email
RECIPIENT="gyn.carlos@gmail.com"
FROM="APP DB Backup"
SUBJECT="ID do cliente: $CLIENT_ID | Arquivo do dia: $BACKUP_FILE"

#Dados do corpo do email

TABLE_OPERATION_ID="Operation ID"
TABLE_OPERATION_ID="Código da operação"

TABLE_CAPTION="Summary"
TABLE_CAPTION="Resumo"

TABLE_STARTED="Started"
TABLE_STARTED="Iniciado"

TABLE_TIME_SPENT="Time spent (seconds)"
TABLE_TIME_SPENT="Tempo gasto (em segundos)"

TABLE_LENGTH_FILE="Length (Mega bytes)"
TABLE_LENGTH_FILE="Tamanho do arquivo (em mega bytes)"

#Dados da operacao (identificacao e logs)
HISTORY=/var/lib/postgresql/logs
UUID=$(cat /proc/sys/kernel/random/uuid)

```

### Execução

1 Passo - Subindo o app
```sh
$ cd bk
$ make up
```

2 Passo - Realizando um backup
```sh
$ cd bk
$ make backup
```

1.  Cada arquivo de backup gerado será salvo na pasta bk/backup com o nome configurado na variável $BACKUP_FILE

1.  Ao término do backup um email de notificação será enviado para o email configurado na variável $RECIPIENT

3 Passo (opcional) - Parando o app

```sh
$ cd bk
$ make down
```

4 Passo - Fazendo um restore:
```sh
$ cd bk
$ make restore
```
Basta executar o comando acima e o restore restaurará o ultimo backup salvo na pasta bk/backups

License
----

MIT


**Free Software**
